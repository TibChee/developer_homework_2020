# README #

You can clone the project from [here](https://TibChee@bitbucket.org/TibChee/developer_homework_2020.git).

### What is this repository for? ###

This is for the Emarsys Developer Homework

### How do I get set up? ###

* Clone it as a Maven project
* Run the main function or the tests
* Note: to be able to run the unit tests you should add the following VM argument: -Dtestng.dtd.http=true