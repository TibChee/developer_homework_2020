package calculator;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.testng.Assert.assertEquals;

public class BasicDueDateCalculatorTest {

    private static final LocalTime START_OF_WORK_DAY = LocalTime.of(9, 0);
    private static final LocalTime END_OF_WORL_DAY = LocalTime.of(17, 0);

    private static final LocalDate MONDAY = LocalDate.parse("2020-08-10");
    private static final LocalDate SUNDAY = LocalDate.parse("2020-08-16");
    private static final LocalTime TEN_AM = LocalTime.of(10, 0, 0);
    private static final LocalTime EIGHT_PM = LocalTime.of(20, 0, 0);

    private BasicDueDateCalculator basicDueDateCalculator;

    private LocalDateTime submitDateTime;
    private int turnaroundTime;

    private LocalDateTime expected;

    @BeforeMethod
    public void setUp() {
        basicDueDateCalculator = BasicDueDateCalculator.create(START_OF_WORK_DAY, END_OF_WORL_DAY);
    }


    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^Submit time cannot be null!$")
    public void testWhenGivenDateIsNull() {
        givenSubmitDateTime(null);
        givenTurnaroundTime(1);
        whenCalculateDueDateMethodIsCalled();
    }

    @Test(expectedExceptionsMessageRegExp = "^Turnaround time should be at least 1.$")
    public void testWhenTurnaroundTimeIsNegative() {
        givenSubmitDateTime(LocalDateTime.of(MONDAY, TEN_AM));
        givenTurnaroundTime(-1);
    }

    @Test(expectedExceptionsMessageRegExp = "^Turnaround time should be at least 1.$")
    public void testWhenTurnaroundTimeIsZero() {
        givenSubmitDateTime(LocalDateTime.of(MONDAY, TEN_AM));
        givenTurnaroundTime(0);
    }

    @DataProvider
    private Object[][] getNegativeTestDates() {
        return new Object[][]{
                {LocalDateTime.of(MONDAY, EIGHT_PM), 1},
                {LocalDateTime.of(SUNDAY, TEN_AM), 1},
                {LocalDateTime.of(SUNDAY, EIGHT_PM), 1}
        };
    }

    @Test(dataProvider = "getNegativeTestDates",
            expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^Submit datetime must be in working hours.$")
    public void testWrongInput(LocalDateTime testDate, int turnaroundTime) {
        givenSubmitDateTime(testDate);
        givenTurnaroundTime(turnaroundTime);
        whenCalculateDueDateMethodIsCalled();
    }

    @DataProvider
    private Object[][] getValidDatesAndTurnaroundTimes() {
        return new Object[][] {
                {LocalDateTime.of(2020, 8, 10, 9, 0), 1, LocalDateTime.of(2020, 8, 10, 10, 0)},
                {LocalDateTime.of(2020, 8, 11, 12, 1), 12, LocalDateTime.of(2020, 8, 12, 16, 1)},
                {LocalDateTime.of(2020, 8, 11, 12, 1), 15, LocalDateTime.of(2020, 8, 13, 11, 1)},
                {LocalDateTime.of(2020, 8, 14, 14, 1), 5, LocalDateTime.of(2020, 8, 17, 11, 1)},
                {LocalDateTime.of(2020, 8, 12, 16, 59), 1, LocalDateTime.of(2020, 8, 13, 9, 59)},
                {LocalDateTime.of(2020, 8, 12, 16, 59), 121, LocalDateTime.of(2020, 9, 3, 9, 59)}
        };
    }

    @Test(dataProvider = "getValidDatesAndTurnaroundTimes")
    public void testNormalOperation(LocalDateTime submitDateTime, int turnaroundTime, LocalDateTime expected) {
        givenSubmitDateTime(submitDateTime);
        givenTurnaroundTime(turnaroundTime);
        whenCalculateDueDateMethodIsCalled();
        thenResultShouldBe(expected);
    }

    private void givenSubmitDateTime(LocalDateTime submitDateTime) {
        this.submitDateTime = submitDateTime;
    }

    private void givenTurnaroundTime(int turnaroundTime) {
        this.turnaroundTime = turnaroundTime;
    }


    private void whenCalculateDueDateMethodIsCalled() {
        expected = basicDueDateCalculator.calculateDueDate(submitDateTime, turnaroundTime);
    }


    private void thenResultShouldBe(LocalDateTime expected) {
        assertEquals(this.expected, expected);
    }
}