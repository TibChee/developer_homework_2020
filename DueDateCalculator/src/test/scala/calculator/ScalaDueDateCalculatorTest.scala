package calculator

import java.time.{LocalDateTime, LocalTime}

import org.testng.Assert.assertEquals
import org.testng.annotations.Test

@Test
class ScalaDueDateCalculatorTest {

  private val START_OF_WORK_DAY = LocalTime.of(9, 0);
  private val END_OF_WORk_DAY = LocalTime.of(17, 0);

  private val scalaDueDateCalculator = new ScalaDueDateCalculator(START_OF_WORK_DAY, END_OF_WORk_DAY)


  @Test
  def testNormalOperation(): Unit = {
    val result = scalaDueDateCalculator.calculateDueDate(LocalDateTime.of(2020, 8, 10, 9, 0), 1)
    assertEquals(result, Some(LocalDateTime.of(2020, 8, 10, 10, 0)))
  }

}
