package calculator

import java.time.DayOfWeek.{SATURDAY, SUNDAY}
import java.time.temporal.ChronoUnit.HOURS
import java.time.{LocalDateTime, LocalTime}

import scala.annotation.tailrec

class ScalaDueDateCalculator(startOfWorkDay: LocalTime, endOfWorkDay: LocalTime) {

  private val WORKING_HOURS = HOURS.between(startOfWorkDay, endOfWorkDay);

  def calculateDueDate(submitTime: LocalDateTime, turnaroundTime: Int): Option[LocalDateTime] = {
    if (!isInWorkingHours(submitTime) || turnaroundTime < 0) None
    else Some(calculate(submitTime, turnaroundTime))
  }

  private def isInWorkingHours(submitDateTime: LocalDateTime) = isInWorkingDay(submitDateTime) && isBetweenWorkingHours(submitDateTime)

  private def isInWorkingDay(submitDateTime: LocalDateTime) = {
    val dayOfWeek = submitDateTime.getDayOfWeek
    (dayOfWeek != SATURDAY) && (dayOfWeek != SUNDAY)
  }

  private def isBetweenWorkingHours(submitDateTime: LocalDateTime) = {
    val hour = submitDateTime.getHour
    val minute = submitDateTime.getMinute
    val submitTime = LocalTime.of(hour, minute)
    !submitTime.isBefore(startOfWorkDay) && !submitTime.isAfter(endOfWorkDay)
  }

  @tailrec
  private def calculate(submitTime: LocalDateTime, numberOfHours: Int): LocalDateTime = {
    if (numberOfHours == 0) submitTime
    else {
      val newTime = if (isBetweenWorkingHours(submitTime.plusHours(1))) {
        submitTime.plusHours(1)
      } else {
        submitTime.plusDays(1).minusHours(WORKING_HOURS - 1)
      }
      calculate(newTime, numberOfHours - 1)
    }
  }
}
