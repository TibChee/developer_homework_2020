package calculator;

import java.time.LocalDateTime;

public interface DueDateCalculator {

    LocalDateTime calculateDueDate(LocalDateTime submitTime, int turnaroundTime);
}
