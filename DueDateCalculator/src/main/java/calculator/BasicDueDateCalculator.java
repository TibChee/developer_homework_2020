package calculator;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;
import static java.time.temporal.ChronoUnit.HOURS;

public class BasicDueDateCalculator implements DueDateCalculator {

    private final LocalTime startOfWorkDay;
    private final LocalTime endOfWorkDay;
    private final long workingDayDuration;

    public static BasicDueDateCalculator create(LocalTime startOfWorkDay, LocalTime endOfWorkDay) {
        if (startOfWorkDay == null || endOfWorkDay == null) throw new IllegalArgumentException("The given parameters cannot be null!");
        if (!endOfWorkDay.isAfter(startOfWorkDay)) throw new IllegalArgumentException("endOfWorkDay must be after startOfWorkDay!");

        long workingDayDuration = HOURS.between(startOfWorkDay, endOfWorkDay);
        return new BasicDueDateCalculator(startOfWorkDay, endOfWorkDay, workingDayDuration);
    }

    private BasicDueDateCalculator(LocalTime startOfWorkDay, LocalTime endOfWorkDay, long workingDayDuration) {
        this.startOfWorkDay = startOfWorkDay;
        this.endOfWorkDay = endOfWorkDay;
        this.workingDayDuration = workingDayDuration;
    }

    @Override
    public LocalDateTime calculateDueDate(LocalDateTime submitDateTime, int turnaroundTime) {
        if (submitDateTime == null) throw new IllegalArgumentException("Submit time cannot be null!");
        if (turnaroundTime <= 0) throw new IllegalArgumentException("Turnaround time should be at least 1.");
        if (!isInWorkingHours(submitDateTime)) throw new IllegalArgumentException("Submit datetime must be in working hours.");

        return calculate(submitDateTime, turnaroundTime);
    }

    private boolean isInWorkingHours(LocalDateTime submitDateTime) {
        return isInWorkingDay(submitDateTime) && isBetweenWorkingHours(submitDateTime);
    }

    private boolean isInWorkingDay(LocalDateTime submitDateTime) {
        DayOfWeek dayOfWeek = submitDateTime.getDayOfWeek();

        return (dayOfWeek != SATURDAY && dayOfWeek != SUNDAY);
    }

    private boolean isBetweenWorkingHours(LocalDateTime submitDateTime) {
        int hour = submitDateTime.getHour();
        int minute = submitDateTime.getMinute();

        LocalTime submitTime = LocalTime.of(hour, minute);

        return !submitTime.isBefore(startOfWorkDay) && !submitTime.isAfter(endOfWorkDay);
    }


//    private LocalDateTime calculate(LocalDateTime submitDateTime, int turnaroundTime) {
//        LocalDateTime nextHour = submitDateTime;
//        while (turnaroundTime > 0) {
//            nextHour = nextHour.plusHours(1);
//            if (!isInWorkingHours(nextHour)) continue;
//            turnaroundTime--;
//        }
//        return nextHour;
//    }

    // a bit less readable but more performant
    private LocalDateTime calculate(LocalDateTime submitDateTime, int turnaroundTime) {
        long days = turnaroundTime / workingDayDuration;
        long hours = turnaroundTime % workingDayDuration;

        LocalDateTime nextDay = submitDateTime;
        while (days > 0) {
            nextDay = nextDay.plusDays(1);
            if (!isInWorkingDay(nextDay)) continue;
            days--;
        }

        while (hours > 0) {
            nextDay = nextDay.plusHours(1);
            if (!isBetweenWorkingHours(nextDay) || !isInWorkingDay(nextDay)) continue;
            hours--;
        }

        return nextDay;
    }

}
