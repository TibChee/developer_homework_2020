package example;

import calculator.BasicDueDateCalculator;
import calculator.DueDateCalculator;
import calculator.ScalaDueDateCalculator;
import scala.Option;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Main {

    public static void main(String[] args) {
        LocalTime startOfWorkDay = LocalTime.of(9, 0);
        LocalTime endOfWorkDay = LocalTime.of(17, 0);
        LocalDateTime submitDate = LocalDateTime.of(2020, 1, 1, 10, 1, 1);

        DueDateCalculator dueDateCalculator = BasicDueDateCalculator.create(startOfWorkDay, endOfWorkDay);
        ScalaDueDateCalculator scalaDueDateCalculator = new ScalaDueDateCalculator(startOfWorkDay, endOfWorkDay);

        LocalDateTime dueDate = dueDateCalculator.calculateDueDate(submitDate, 10);
        Option<LocalDateTime> scalaDueDate = scalaDueDateCalculator.calculateDueDate(null, 10);

        System.out.println("TestDate: " + submitDate);
        System.out.println();
        System.out.println("Java: " + dueDate);
        System.out.println("Scala: " + scalaDueDate);
    }
}
